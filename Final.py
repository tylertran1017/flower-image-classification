import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from keras.models import Sequential, load_model
from keras.layers import Dense, Flatten, Dropout, BatchNormalization
from keras.utils import image_dataset_from_directory
from keras.applications import VGG19
from keras.optimizers import Adam
from sklearn.metrics import (
    confusion_matrix,
    classification_report,
    ConfusionMatrixDisplay,
)

MODEL_FILEPATH = "models/model.h5"
IMG_DIR = "flowers"
CUSTOM_DIR = "custom_meme"
IMG_SIZE = 224

NUM_EPOCH = 12
RANDOM_SEED = 0
BATCH_SIZE = 32
TRAIN_SPLIT = 0.7
VALIDATION_SPLIT = 0.15
LEARNING_RATE = 0.001

LABELS = os.listdir(IMG_DIR)
NUM_LABELS = len(LABELS)
LABEL_COLORS = plt.cm.PuBuGn(np.linspace(0.2, 1.0, NUM_LABELS))


def get_data(dir, split=True):
    full_dataset = image_dataset_from_directory(
        dir,
        label_mode="categorical",
        batch_size=BATCH_SIZE,
        image_size=(IMG_SIZE, IMG_SIZE),
        shuffle=True,
        seed=RANDOM_SEED,
    ).prefetch(buffer_size=tf.data.AUTOTUNE)

    if not split:
        return full_dataset

    dataset_size = len(full_dataset)
    train_size = int(TRAIN_SPLIT * dataset_size)
    val_size = int(VALIDATION_SPLIT * dataset_size)

    train_dataset = full_dataset.take(train_size)
    val_dataset = full_dataset.skip(train_size).take(val_size)
    test_dataset = full_dataset.skip(train_size + val_size)

    return train_dataset, val_dataset, test_dataset


def build_model(training_data, validation_data):
    print("BUILDING MODEL")
    pre_trained_model = VGG19(
        input_shape=(IMG_SIZE, IMG_SIZE, 3), include_top=False, weights="imagenet"
    )
    for layer in pre_trained_model.layers:
        layer.trainable = False

    model = Sequential(
        [
            pre_trained_model,
            Flatten(),
            Dense(128, activation="relu"),
            BatchNormalization(),
            Dropout(0.5),
            Dense(5, activation="softmax"),
        ]
    )
    model.compile(
        optimizer=Adam(lr=LEARNING_RATE),
        loss="categorical_crossentropy",
        metrics=["accuracy"],
    )
    history = model.fit(
        training_data, epochs=NUM_EPOCH, validation_data=validation_data
    )
    plot_history(history)
    model.save(MODEL_FILEPATH)


def plot_history(history):
    fig, ax = plt.subplots(2, 1)
    ax[0].plot(history.history["accuracy"], "magenta")
    ax[0].plot(history.history["val_accuracy"], "teal")
    ax[0].set_title("Model Accuracy")
    ax[0].set_ylabel("Accuracy")
    ax[0].set_xlabel("Epoch")
    ax[0].legend(["Train", "Validation"], loc="upper left")

    ax[1].plot(history.history["loss"], "magenta")
    ax[1].plot(history.history["val_loss"], "teal")
    ax[1].set_title("Model Loss")
    ax[1].set_ylabel("Loss")
    ax[1].set_xlabel("Epoch")
    ax[1].legend(["Train", "Validation"], loc="upper left")

    fig.subplots_adjust(hspace=0.6)


def plot_histogram(data: tf.data.Dataset, title):
    class_counts = np.zeros(NUM_LABELS)
    for _, label in data.unbatch():
        label = np.argmax(label)
        class_counts[label] += 1
    plt.figure()
    plt.title(title)
    plt.ylabel("Number of Samples")
    plt.bar(LABELS, class_counts, color=LABEL_COLORS)


def plot_confusion_matrix(y_true, y_pred, title):
    disp = ConfusionMatrixDisplay(
        confusion_matrix(y_true, y_pred), display_labels=LABELS
    )
    disp.plot(cmap="Blues")
    disp.ax_.set_title(title)


def plot_model_result(model, data: tf.data.Dataset):
    fig, ax = plt.subplots(3, 4, figsize=(12, 8))
    images, labels = next(iter(data))
    pred = model(images)
    for i in range(12):
        predict_label = LABELS[np.argmax(pred[i])]
        actual_label = LABELS[np.argmax(labels[i])]
        ax[i // 4][i % 4].imshow(images[i].numpy().astype("uint8"))
        ax[i // 4][i % 4].set_title(f"True: {actual_label} | Pred: {predict_label}")
    fig.tight_layout()


def predict_labels(model, data: tf.data.Dataset):
    y_pred = []
    y_true = []
    for images, labels in data:
        pred = model(images)
        y_pred.append(np.argmax(pred, axis=1))
        y_true.append(np.argmax(labels, axis=1))
    y_pred = np.concatenate(y_pred, axis=0)
    y_true = np.concatenate(y_true, axis=0)
    return y_true, y_pred


def run(training_data, validation_data, test_data, custom_data):
    if not os.path.exists(MODEL_FILEPATH):
        build_model(training_data, validation_data)
    model = load_model(MODEL_FILEPATH)
    model.summary()

    y_true_test, y_pred_test = predict_labels(model, test_data)
    print("Test data classfication report:")
    print(classification_report(y_true_test, y_pred_test, target_names=LABELS))
    plot_confusion_matrix(y_true_test, y_pred_test, "Test Confusion Matrix")
    print(f"Loss and Accuracy: {model.evaluate(test_data)}")

    # y_true_train, y_pred_train = predict_labels(model, training_data)
    # print("Train data classfication report:")
    # print(classification_report(y_true_train, y_pred_train, target_names=LABELS))
    # plot_confusion_matrix(y_true_train, y_pred_train, "Train Confusion Matrix")
    # print(f"Loss and Accuracy: {model.evaluate(training_data)}")

    y_true_custom, y_pred_custom = predict_labels(model, custom_data)
    print("Custom data classfication report:")
    print(classification_report(y_true_custom, y_pred_custom, target_names=LABELS))
    plot_confusion_matrix(y_true_custom, y_pred_custom, "Custom Confusion Matrix")
    plot_model_result(model, custom_data)
    print(f"Loss and Accuracy: {model.evaluate(custom_data)}")

    plot_histogram(training_data, "Training data sample distribution")
    plot_histogram(validation_data, "Validation data sample distribution")
    plot_histogram(test_data, "Test data sample distribution")
    plot_histogram(custom_data, "Custom data sample distribution")
    plt.show()


if __name__ == "__main__":
    training_data, validation_data, test_data = get_data(IMG_DIR)
    custom_data = get_data(CUSTOM_DIR, split=False)

    if len(sys.argv) > 1 and sys.argv[1] == "build":
        build_model(training_data, validation_data)
    run(training_data, validation_data, test_data, custom_data)
