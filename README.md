# Table of Contents
- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Methodology](#methodology)
  - [Problem Setup](#problem-setup)
  - [Neural Network Design](#neural-network-design)
  - [Backpropogation and Loss Function](#backpropogation-and-loss-function)
  - [Training \& Testing Scheme](#training--testing-scheme)
- [Results and Discussion](#results-and-discussion)
  - [Model Summary](#model-summary)
  - [Testing and Training Results](#testing-and-training-results)
  - [Custom Input Data](#custom-input-data)
  - [Overall Effectiveness](#overall-effectiveness)
- [Conclusion](#conclusion)
- [References](#references)

# Introduction
The task at hand is to create a convolutional neural network to classify the images from the [Kaggle Flower Recognition dataset](https://www.kaggle.com/datasets/alxmamaev/flowers-recognition) [1]. This dataset consists of 4242 images of flowers which are divided into five classes: daisy, tulip, rose, sunflower, dandelion. There are approximately 800 photos for each class each with an approximate resolution of 320x240 pixels. The neural network designed will utilize the Keras framework within the TensorFlow Python library to do image classification on the dataset; the goal being to train a neural network to correctly classify flowers to the aforementioned classes.

# Methodology
## Problem Setup
The problem consists of designing a layered neural network which is then trained on the dataset in [1]. The entire dataset will be used and divided in the following manner:

| Dataset    | Percentage |
| ---------- | ---------- |
| Training   | 70%        |
| Validation | 15%        |
| Testing    | 15%        |

**Table 1**: Dataset Division

70% of the data will be used for training the neural network to update the weight and biases, 15% will be used for validation to verify that the neural network is not overfitting for the dataset, and the remaining 15% will be used to test the efficacy of the neural network. This last 15% can be used to obtain performance metrics. All images will be resized to 244x244 pixels.

## Neural Network Design
Transfer learning will be used to design the neural network; specifically, the neural network design will leverage the Visual Geometry Group’s 19 layer neural network VGG19 [4]. The motivation is to take advantage of a robust and proven network to improve the effectiveness of the image classifier and to augment this neural network for the Flower Recognition use case.

The following neural network diagram in Figure 1 represents the neural network described and is illustrated using the NN SVG tool [2]. The network contains the VGG19 network which is modeled in reference to the illustration in [3]

| ![Figure 1](assets/nn_angle_edit.png) |
| :-----------------------------------: |
| **Figure 1**: Neural Network Diagram  |

Total params: 23,236,933
Trainable params: 3,212,293
Non-trainable params: 20,024,640
| Layer (type)                             | Output Shape      | Param #  |
| ---------------------------------------- | ----------------- | -------- |
| vgg19 (Functional)                       | (None, 7, 7, 512) | 20024384 |
| flatten (Flatten)                        | (None, 25088)     | 0        |
| dense (Dense)                            | (None, 128)       | 3211392  |
| batch_normalization (BatchNormalization) | (None, 128)       | 512      |
| dropout (Dropout)                        | (None, 128)       | 0        |
| dense_1 (Dense)                          | (None, 5)         | 645      |

**Table 2**: Neural network summary 

As seen in Table 2, the VGG19 network is abstracted to a “black box” and will accept a 244x244x3 tensor corresponding to the images. The output of the VGG19 black box is then flattened, trained on a 128 neuron dense layer with the “ReLU” activation function, batch normalized, a dropout function is applied, and then the output layer is 5 neurons with a “Softmax” activation function. The choice of using a 128 neuron dense layer is to have trainable weights before the output to augment the VGG 19 network to be able to classify the flow images since the VGG19 network is left fixed. The use of the ReLu activation function is to ensure that the output is not a linear combination of the previous layer’s neuron output and ReLU is a common choice in TensorFlow networks. Batch normalization is done to normalize the output from the previous neuron layer to reduce overfitting and accelerate training. Similarly, the dropout layer with a dropout rate of 0.50 is used to prevent overfitting of the data by providing variance in neuron outputs to minimize the loss function. The output layer is five neurons corresponding to the five classes using the softmax activation due to having multiple classes and to constrain the output range between -1 and 1 for effective use with the cross entropy loss function.

## Backpropogation and Loss Function
Backpropagation is the concept of propagating the error of our loss function through the layers of the neural network in order to adjust the weights with the purpose of minimizing the loss function. This is analogous to running iterations to find the local minima of the multidimensional loss function. For the CNN described above and illustrated in Figure 1, there are 3211392 weight parameters for the first 128 neuron dense layer, 512 parameters for the batch normalization layer and 645 parameters for the output layer, these values are tuned to minimize the loss function using the chain rule to determine the sensitivity of each of the weight contributions. 

The loss function used in this neural network is categorical cross entropy which uses one-hot encoding. The loss function is described as follows:
$$
L = 	\sum_{i=1}^{4} -y_klog(y_{est,k})
$$

Where $y_k$ represents the one-hot encoded value in $y$ and $y_{est,k}$ represents the neuron output of the softmax layer. The loss function value is minimized when the output layer closely matches the one-hot encoded target, in which case the loss function would evaluate to values close to 0. For example:

Suppose a flower is class 2:

$$
target=y=[0, 0, 1, 0, 0]
$$

Suppose the output layer approximately classifies the target as class 2:

$$
output=y_{est}=[0, 0.025, 0.95, 0.025, 0]
$$

Then the loss function would evaluate to:

$$
L = -[0 + 0+log(0.95)+0+0] = -log(0.95) \approx 0.0223
$$

Derivatives to the loss function are calculated to determine the sensitivity and using that sensitivity, the loss function is minimized by adjusting the tunable parameters.

## Training & Testing Scheme
| Training details                                            |
| ----------------------------------------------------------- |
| 70% of the total dataset is used for training               |
| 15% of the total dataset is used for validation             |
| The optimizer used is “Adam” with a learning rate of =0.001 |
| The neural network is trained in 12 epochs                  |
| The image data is batched into 32                           |
| The data is shuffled using seed 0                           |

**Table 3**: Training Scheme

| Testing details                                                                                                                                             |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 15% of the total dataset is used for testing. These are images which have not been trained or validated with                                                |
| The testing input will also batch process with batch size 32                                                                                                |
| Other image data not from the original dataset is then tested to test how efficacious the model is: 2x daisy, 3x tulip, 3x rose, 2x sunflower, 2x dandelion |

**Table 4**: Testing Scheme

# Results and Discussion
Figure 2 shows the model accuracy and loss when training the neural network over 12 epochs. Figure 3 and Figure 4 show the distribution of images in training and validation datasets respectively.

|       ![Figure 2](assets/AccuracyAndLoss.png)        |
| :--------------------------------------------------: |
| **Figure 2**: Model accuracy and loss over 12 epochs |

|    ![Figure 3](assets/TrainingDist.png)     |
| :-----------------------------------------: |
| **Figure 3**: Training dataset distribution |

|    ![Figure 4](assets/ValidationDist.png)     |
| :-------------------------------------------: |
| **Figure 4**: Validation dataset distribution |

| Training Parameters | Value                    |
| ------------------- | ------------------------ |
| Learning rate       | 0.001                    |
| Optimizer           | Adam                     |
| Loss function       | Categorical Crossentropy |
| Number of epochs    | 12                       |
| Random seed         | 0                        |
| Trainig samples     | 70% of dataset           |
| Validation samples  | 15% of dataset           |

**Table 5**: Training parameters

## Model Summary
As described by the model summary in Figure 2, the neural network consists of a pertain VGG19 “black box”, a flatten layer, a 128 neuron dense layer with 3211392 parameters, a batch normalization layer with 512 parameters using the “ReLU” activation function, a 0.50 rate dropout layer, and an output 5 neuron output dense layer with 645 parameters using the “Softmax” activation function. The total number of parameters in the neural network is 23,236,933 however 20,023,640 of those parameters are fixed in the VGG leaving 3,212,293 trainable parameters for correct image classification. Figure 5 describes the training logs which correspond to the plots in Figure 2.

```
94/94 [==============================] - 11s 109ms/step - loss: 0.8703 - accuracy: 0.7138 - val_loss: 0.5760 - val_accuracy: 0.8313
Epoch 2/12
94/94 [==============================] - 10s 107ms/step - loss: 0.2876 - accuracy: 0.9036 - val_loss: 0.3828 - val_accuracy: 0.8813
Epoch 3/12
94/94 [==============================] - 10s 107ms/step - loss: 0.1797 - accuracy: 0.9412 - val_loss: 0.4479 - val_accuracy: 0.8703
Epoch 4/12
94/94 [==============================] - 10s 107ms/step - loss: 0.0893 - accuracy: 0.9804 - val_loss: 0.3168 - val_accuracy: 0.9016
Epoch 5/12
94/94 [==============================] - 10s 106ms/step - loss: 0.0593 - accuracy: 0.9867 - val_loss: 0.3673 - val_accuracy: 0.8859
Epoch 6/12
94/94 [==============================] - 10s 106ms/step - loss: 0.0474 - accuracy: 0.9877 - val_loss: 0.3220 - val_accuracy: 0.8984
Epoch 7/12
94/94 [==============================] - 10s 106ms/step - loss: 0.0335 - accuracy: 0.9940 - val_loss: 0.3820 - val_accuracy: 0.8969
Epoch 8/12
94/94 [==============================] - 10s 106ms/step - loss: 0.0261 - accuracy: 0.9953 - val_loss: 0.3779 - val_accuracy: 0.8938
Epoch 9/12
94/94 [==============================] - 10s 106ms/step - loss: 0.0245 - accuracy: 0.9963 - val_loss: 0.4009 - val_accuracy: 0.8938
Epoch 10/12
94/94 [==============================] - 10s 107ms/step - loss: 0.0184 - accuracy: 0.9967 - val_loss: 0.3772 - val_accuracy: 0.8891
Epoch 11/12
94/94 [==============================] - 10s 105ms/step - loss: 0.0132 - accuracy: 0.9980 - val_loss: 0.4425 - val_accuracy: 0.8875
Epoch 12/12
94/94 [==============================] - 10s 105ms/step - loss: 0.0124 - accuracy: 0.9993 - val_loss: 0.3945 - val_accuracy: 0.8906
Model: "sequential"
```
**Figure 5**: Training logs

## Testing and Training Results
The following section will illustrate using the trained model with the testing dataset and training dataset. Figure 6 illustrates the testing dataset sample distribution.

|    ![Figure 6](assets/TrainingDist.png)     |
| :-----------------------------------------: |
| **Figure 6**: Training dataset distribution |

| ![Figure 7](assets/TestConfusionMatrix.png) |
| :-----------------------------------------: |
|     **Figure 7**: Test confusion matrix     |

|              | precision | recall | f1-score | support |
| ------------ | --------- | ------ | -------- | ------- |
| daisy        | 0.84      | 0.86   | 0.85     | 115     |
| dandelion    | 0.88      | 0.91   | 0.90     | 169     |
| rose         | 0.79      | 0.86   | 0.82     | 116     |
| sunflower    | 0.94      | 0.81   | 0.87     | 120     |
| tulip        | 0.87      | 0.85   | 0.86     | 149     |
| accuracy     |           |        | 0.86     | 669     |
| macro avg    | 0.86      | 0.86   | 0.86     | 669     |
| weighted avg | 0.87      | 0.86   | 0.86     | 669     |

**Table 6**: Testing classifcation report

| accuracy          | loss               |
| ----------------- | ------------------ |
| 0.859491765499115 | 0.5268449187278748 |

**Table 7**: Testing accuracy and loss

From the testing classification report in Table 6, it can be seen that the weighted average “precision” and “recall” for the 669 samples is 0.87 and 0.86 respectively. Meaning that the model will not falsely label a sample 87% of the time and that the correct sample will be identified 86% of the time. The confusion matrix in Figure 7 also shows that most samples are classified correctly. Running the model evaluation function provides a training accuracy and loss of roughly 85.9% and 0.527 respectively as shown in Table 7.

| ![Figure 8](assets/TrainConfusionMatrix.png) |
| :------------------------------------------: |
|   **Figure 8**: Training confusion matrix    |

|              | precision | recall | f1-score | support |
| ------------ | --------- | ------ | -------- | ------- |
| daisy        | 1.00      | 1.00   | 1.00     | 552     |
| dandelion    | 1.00      | 1.00   | 1.00     | 741     |
| rose         | 1.00      | 1.00   | 1.00     | 536     |
| sunflower    | 1.00      | 1.00   | 1.00     | 506     |
| tulip        | 1.00      | 1.00   | 1.00     | 673     |
| accuracy     |           |        | 1.00     | 3008    |
| macro avg    | 1.00      | 1.00   | 1.00     | 3008    |
| weighted avg | 1.00      | 1.00   | 1.00     | 3008    |

**Table 8**: Training classifcation report

| accuracy           | loss                 |
| ------------------ | -------------------- |
| 0.9996675252914429 | 0.001604636199772358 |

**Table 9**: Training accuracy and loss

From the training classification report in Table 8, it can be seen that the weighted average “precision” and “recall” for the 3008 samples is 1.00 and 1.00 respectively. This indicates that for all 3008 training samples, running those samples through our model will classify those images with 100% accuracy. This is understandable as these are the sample images used to train the model. Running the model evaluation function provides a training accuracy and loss of roughly 99.9% and 0.002 respectively as shown in Table 9.

## Custom Input Data
As mentioned in the above section, custom photos no apart of the original dataset were tested with the model yielding the following results

|    ![Figure 9](assets/CustomDist.png)     |
| :---------------------------------------: |
| **Figure 9**: Custom dataset distribution |

|    ![Figure 10](assets/Custom_Classification.png)     |
| :---------------------------------------: |
| **Figure 10**: Custom dataset predictions|

|    ![Figure 11](assets/CustomConfusionMatrix.png)     |
| :---------------------------------------: |
| **Figure 11**: Custom dataset confusion matrix |

|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| daisy        | 1.00      | 1.00   | 1.00     | 2       |
| dandelion    | 1.00      | 1.00   | 1.00     | 2       |
| rose         | 1.00      | 1.00   | 1.00     | 3       |
| sunflower    | 1.00      | 1.00   | 1.00     | 2       |
| tulip        | 1.00      | 1.00   | 1.00     | 3       |
| accuracy     |           |        | 1.00     | 12      |
| macro avg    | 1.00      | 1.00   | 1.00     | 12      |
| weighted avg | 1.00      | 1.00   | 1.00     | 12      |

**Table 10**: Custom data classification report

| accuracy           | loss                 |
| ------------------ | -------------------- |
| 0.9999999403953552 | 0.05514449626207352 |

**Table 11**: Custom data accuracy and loss

From the confusion matrix in Figure 11, it seems to be the case that the network performs fairly well on novel data. It can be seen in Figure 10 that the network is able to classify non-photographic images as some of the images are drawn artwork.

## Overall Effectiveness
Overall, the impression is that the image for the purposes of classifying the five specific flower types performs very well. From the testing samples, the accuracy of the neural network seems to be approximately 86% for classifying the flowers. The model accuracy can mostly be attributed to leveraging the robust VGG19 network which is a proven CNN architecture for image recognition. The additional training augments the VGG19 architecture for our flower classification use case.

As for generalizing to images outside of the dataset, the model was able to correctly predict the external images in the custom dataset including images including non-photographic images. This seems to indicate a level of generalized performance of the neural network model. Though it should be noted that the sample size of the custom dataset is small it may very well be the case that the real performance is worse with more nuanced images. Images that are not a part of the original labels will be incorrectly classified.

# Conclusion
In summary, this report documents the design and implementation of a CNN using transfer learning with the VGG19 neural network. The neural network is augmented with a few custom layers at the tail end and ultimately is trained to classify flowers from the Kaggle Flower Recognition dataset [1] into 5 classes. The dataset had been split into training, validation, and testing datasets to train and test the neural network. This ultimately resulted in a CNN that is able to classify the flower correctly with ~86% accuracy. Using a custom dataset which contains different images from the original dataset, the model was able to correctly predict all the images indicating a level of generalized performance. Overall, for the scope of classifying the five specific flowers of daisy, tulip, rose, sunflower, and dandelion, the performance of the network is fairly good.

# References
“Flowers recognition,” Kaggle, Jul. 16, 2021. https://www.kaggle.com/datasets/alxmamaev/flowers-recognition

“NN SVG.” https://alexlenail.me/NN-SVG/LeNet.html

“Fig. 8 Illustration of the network architecture of VGG-19 model: conv...,” ResearchGate. https://www.researchgate.net/figure/llustration-of-the-network-architecture-of-VGG-19-model-conv-means-convolution-FC-means_fig2_325137356

K. Team, “Keras documentation: VGG16 and VGG19.” https://keras.io/api/applications/vgg/